FROM gcc

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git libboost-test-dev libboost-dev libboost-filesystem-dev\
    libboost-program-options-dev libboost-system-dev libboost-thread-dev libgtest-dev\
    google-mock libcairo2-dev libglade2-dev libglib2.0-dev libgtk2.0-dev libtiff5-dev\
    pkg-config doxygen graphviz ccache libc6-dev python-pip
RUN pip install gcovr==4.2
