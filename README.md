# Scroom CI

Docker images for use with GitLab CI to build the Scroom repository and Scroom plugins. Prebuilt images can be found in the `Packages & Registries` section of this repository.

## Linux

See the [.gitlab-ci.yml](/.gitlab-ci.yml) file for instructions on how to build the Linux image.

## Windows

To build the Windows image an MSYS2 install with all the required packages is required. Refer to the [ScroomPlusPlus Wiki](https://gitlab.com/sep-group-2/scroom/-/wikis/Setting-up-a-Windows-Development-Environment) for more details.

Building the image is then done by first coping the MSYS2 install to the current directory and then building the image.

```
cp -r C:\\msys64 msys64
docker build -t $IMAGE:windows .
docker login -u $USER -p $PASSWORD $REGISTRY
docker push $IMAGE:windows
```

## History
Development started on the 26th of April, 2020.